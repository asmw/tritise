from tritise import Tritise, __version__

import unittest
import os
import tempfile
import shutil
from dateutil.parser import parse as parse_date

TEST_POINTS = [
    {'value' : 1, 'timestamp' : parse_date('1. Jan 2010')},
    {'value' : 2, 'timestamp' : parse_date('2. Feb 2010')},
    {'value' : 3, 'timestamp' : parse_date('3. Mar 2010')},
    {'value' : 4, 'timestamp' : parse_date('4. Apr 2010')},
    {'value' : 4, 'timestamp' : parse_date('1. Dec 2010'), 'tag' : 'tag'},
]

class TestVersion(unittest.TestCase):
    def test_version(self):
        self.assertEqual(__version__, '0.1.0')

class TestTritise(unittest.TestCase):
    def setUp(self):
        self.test_dir = tempfile.mkdtemp()
        self.orig_cwd = os.getcwd()
        os.chdir(self.test_dir)
        print('Running tests in %s' % os.getcwd())

    def tearDown(self):
        os.chdir(self.orig_cwd)
        shutil.rmtree(self.test_dir)

    def test_create_file(self):
        """
        Creating a Tritise object without a filename
        should create a tritise.sqlite database in the current working directory
        """
        t = Tritise()
        self.assertTrue(os.path.isfile(t._filename))

    def test_create_with_name(self):
        t = Tritise(':memory:')

    def test_empty(self):
        """An empty timeseries has no last entry"""
        t = Tritise(':memory:')
        self.assertIsNone(t.last())

    def test_add(self):
        """Adding data should succeed and create a new data point with an id"""
        t = Tritise(':memory:')
        value = 0.1
        result = t.add(value)
        self.assertIsNotNone(result)
        self.assertIsNotNone(result.id)

    def test_last(self):
        """
        The newest data point should be returned,
        if timestamps are euqal, return the largest id
        """
        t = Tritise(':memory:')
        last_value = 100
        t.add(0.1)
        t.add(last_value)
        self.assertEqual(last_value, t.last().value)


    def test_add_with_date(self):
        """Adding with a specific date should insert a record with that date"""
        date = parse_date('21. Jan 2010')
        t = Tritise(':memory:')
        value = 6
        t.add(value, timestamp=date)

    def test_tags(self):
        """Tags distinguish series in a database"""
        t = Tritise(':memory:')
        value_notag = 1
        value_tag = 2
        t.add(value_notag)
        t.add(value_tag, tag='tagged')
        self.assertEqual(value_tag, t.last('tagged').value)
        self.assertEqual(value_notag, t.last().value)

    def test_add_many(self):
        """Adding a bunch of existing data points must be possible"""
        t = Tritise(':memory:')
        t.add_many(TEST_POINTS)
        self.assertEqual(TEST_POINTS[-1]['value'], t.last().value)

    def test_count(self, tag = None):
        """Count all entries with an optional tag"""
        t = Tritise(':memory:')
        t.add_many(TEST_POINTS)
        self.assertEqual(4, t.count())
        self.assertEqual(1, t.count('tag'))

    def test_get_all(self):
        """
        All entries with a tag can be fetched
        They are returned in descending order by timestamp and id 
        """
        t = Tritise(':memory:')
        t.add_many(TEST_POINTS)
        result = t.all()
        self.assertEqual(4, len(result))
        result = t.all('tag')
        self.assertEqual(1, len(result))

    def test_range(self):
        """Fetching a range should return all data points included in the limits"""
        t = Tritise(':memory:')
        t.add_many(TEST_POINTS)
        start_stamp = TEST_POINTS[1]['timestamp']
        end_stamp = TEST_POINTS[2]['timestamp']
        result = t.range(start_date=start_stamp, end_date=end_stamp)
        self.assertEqual(2, len(result))


if __name__ == '__main__':
    unittest.main()
